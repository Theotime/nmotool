/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:07:30 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 16:07:31 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_mapfile	*ft_map_file(char *path)
{
	t_mapfile		*map;
	struct stat		buff;

	if ((map = ft_memalloc(sizeof(t_mapfile))) == NULL)
		return (NULL);
	map->name = ft_strdup(path);
	map->error = 1;
	if ((map->fd = open(path, O_RDONLY)) < 0)
		return (map);
	if (fstat(map->fd, &buff) < 0)
		return (map);
	map->size = buff.st_size;
	map->content = mmap(0, map->size, PROT_READ, MAP_PRIVATE, map->fd, 0);
	if (map->content == MAP_FAILED)
		return (map);
	map->error = 0;
	return (map);
}
