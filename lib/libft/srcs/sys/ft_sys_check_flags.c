/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_check_flags.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:07 by triviere          #+#    #+#             */
/*   Updated: 2015/12/24 00:53:32 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_sys_flag_is_flag_format(t_el *flag)
{
	char		*arg;
	int			len;

	arg = ((char*)flag->data);
	len = ft_strlen(arg);
	if (len > 1)
	{
		if (arg[0] == '-')
		{
			if (ft_isalpha(arg[1]))
				return (1);
			else if (arg[1] == '-' && len > 2 && ft_isalpha(arg[2]))
				return (-1);
			else if (arg[1] == '-' && !arg[2])
				return (0);
		}
	}
	return (0);
}

t_el		*ft_sys_check_long_flag(t_el *el)
{
	t_sys		*sys;
	t_el		*next;

	sys = ft_get_sys();
	next = NULL;
	if (ft_sys_flag_parse_lflag_params(el))
	{
		next = el->next;
		el = ft_lst_remove(sys->av, el);
		if (el)
			free(el);
	}
	else
		sys->bad_flag(((char*)el->data) + 2);
	return (next);
}

t_el		*ft_sys_check_short_flag(t_el *el)
{
	t_sys		*sys;
	t_el		*next;
	char		*flag;

	sys = ft_get_sys();
	flag = ((char*)el->data);
	while (*(++flag))
	{
		if (!ft_sys_flag_parse_sflag_params(el, flag))
		{
			flag[1] = '\0';
			sys->bad_flag(flag);
		}
	}
	next = el->next;
	el = ft_lst_remove(sys->av, el);
	if (el)
		free(el);
	return (next);
}

void		ft_sys_check_flags(void)
{
	t_sys		*sys;
	t_el		*cur;
	int			is_flag;

	sys = ft_get_sys();
	cur = sys->av->start;
	while (cur)
	{
		is_flag = ft_sys_flag_is_flag_format(cur);
		if (!ft_strcmp(((char*)cur->data), "--"))
		{
			cur = ft_lst_remove(sys->av, cur);
			free(cur);
			break ;
		}
		else if (is_flag > 0)
			cur = ft_sys_check_short_flag(cur);
		else if (is_flag < 0)
			cur = ft_sys_check_long_flag(cur);
		else
			break ;
	}
}
