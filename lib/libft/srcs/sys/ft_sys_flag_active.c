/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_flag_active.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:24 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:14:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sys_flag_active_sflag(char flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (((t_flag*)cur->data)->sname == flag)
		{
			((t_flag*)cur->data)->active = 1;
			return ;
		}
		cur = cur->next;
	}
}

void	ft_sys_flag_active_lflag(char *flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (ft_strcmp(((t_flag*)cur->data)->lname, flag) == 0)
		{
			((t_flag*)cur->data)->active = 1;
			return ;
		}
		cur = cur->next;
	}
}

int		ft_sys_flag_sflag_active(char flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (((t_flag*)cur->data)->sname == flag)
			return (((t_flag*)cur->data)->active);
		cur = cur->next;
	}
	return (-1);
}

int		ft_sys_flag_lflag_active(char *flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (ft_strcmp(((t_flag*)cur->data)->lname, flag) == 0)
			return (((t_flag*)cur->data)->active);
		cur = cur->next;
	}
	return (-1);
}
