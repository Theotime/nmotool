/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_log.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:45 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:10:47 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_sys_debug(char *str)
{
	t_sys		*sys;

	sys = ft_get_sys();
	if (sys->lflag_active("debug"))
		ft_putendl(str);
}

void		ft_sys_log(char *str)
{
	t_sys		*sys;

	sys = ft_get_sys();
	if (sys->lflag_active("verbose"))
		ft_putendl(str);
}

void		ft_sys_error(char *str)
{
	ft_putendl_fd(str, 2);
}

void		ft_sys_die(char *str)
{
	t_sys	*sys;

	sys = ft_get_sys();
	sys->error(str);
	exit(1);
}
