/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:09:56 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:16:03 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_sys		*ft_get_sys(void)
{
	static t_sys	sys;
	static int		init = 0;

	if (!init)
	{
		ft_bzero((void *)&sys, sizeof(sys));
		ft_sys_init_fn(&sys);
		init = 1;
		ft_sys_init_flags(&sys);
	}
	return (&sys);
}

void		*ft_sys_get_pgm(void)
{
	t_sys	*sys;

	sys = ft_get_sys();
	return (sys->pgm);
}
