/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:07:49 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 16:08:55 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FILE_H
# define FT_FILE_H

# include <sys/mman.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct s_mapfile	t_mapfile;

struct						s_mapfile
{
	char					*name;
	int						size;
	int						fd;
	void					*content;
	char					error;
};

t_mapfile					*ft_map_file(char *path);

void						ft_map_file_free(t_mapfile *file);

#endif
