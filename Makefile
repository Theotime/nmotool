CC				= gcc
FLAGS			= -Wall -Werror -Wextra -g3
SRC_DIR			= src
OBJ_DIR			= obj
INCLUDES		= -Ilib/libft/includes
LDFLAGS			= -L lib/libft -lft

NM_DIR_SRC		= $(SRC_DIR)/nm
NM_DIR_OBJ		= $(OBJ_DIR)/nm
NM_SRCS			= $(shell find $(NM_DIR_SRC) -type f)
NM_INCLUDES		= $(INCLUDES) -Iinclude/nm
NM_CFLAGS		= $(FLAGS) $(NM_INCLUDES)
NM_NAME			= ft_nm
NM_OBJS			= $(patsubst $(NM_DIR_SRC)/%,$(NM_DIR_OBJ)/%, $(NM_SRCS:.c=.o))

OTOOL_DIR_SRC	= $(SRC_DIR)/otool
OTOOL_DIR_OBJ	= $(OBJ_DIR)/otool
OTOOL_SRCS		= $(shell find $(OTOOL_DIR_SRC) -type f)
OTOOL_INCLUDES	= $(INCLUDES) -Iinclude/otool
OTOOL_CFLAGS	= $(FLAGS) $(OTOOL_INCLUDES)
OTOOL_NAME		= ft_otool
OTOOL_OBJS		= $(patsubst $(OTOOL_DIR_SRC)/%,$(OTOOL_DIR_OBJ)/%, $(OTOOL_SRCS:.c=.o))

all: lib_all $(NM_NAME) $(OTOOL_NAME)
	@echo > /dev/null

re : fclean all

clean: lib_clean
	@rm -rf obj

fclean: lib_fclean clean
	@rm -rf ${NM_NAME}
	@rm -rf ${OTOOL_NAME}

$(NM_NAME): $(NM_OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

$(NM_DIR_OBJ)/%.o : $(NM_DIR_SRC)/%.c include/nm/ft_nm.h
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(NM_CFLAGS)

$(OTOOL_NAME): $(OTOOL_OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OTOOL_DIR_OBJ)/%.o : $(OTOOL_DIR_SRC)/%.c include/otool/ft_otool.h
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(OTOOL_CFLAGS)

lib_all:
	@make -C lib/libft

lib_clean:
	@make clean -C lib/libft

lib_fclean:
	@make fclean -C lib/libft

.PHONY: clean fclean re all
