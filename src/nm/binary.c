/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:39:10 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 17:54:06 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

t_binary		*ft_binary_init(void)
{
	t_binary	*bin;

	bin = ft_memalloc(sizeof(t_binary));
	bin->cmds = ft_lst_init();
	bin->file = NULL;
	bin->arch = 0;
	bin->text = NO_SECT;
	bin->data = NO_SECT;
	bin->bss = NO_SECT;
	bin->k = 0;
	return (bin);
}

static int		ft_swap32(uint32_t num)
{
	return (((num >> 24) & 0xff) | ((num << 8) & 0xff0000) |
		((num >> 8) & 0xff00) | ((num << 24) & 0xff000000));
}

static void		ft_start_fat(t_binary *bin)
{
	struct fat_arch			*fat;
	int						cpu_type;

	fat = (struct fat_arch*)((char*)bin->file->content + \
		sizeof((struct fat_arct*)bin->file->content));
	cpu_type = ft_swap32(CPU_TYPE_X86_64);
	while (fat->cputype != cpu_type)
		fat = (void*)fat + sizeof(*fat);
	bin->file->content += ft_swap32(fat->offset);
	bin->arch = *(int*)bin->file->content;
	if (bin->arch == MH_MAGIC_64)
		ft_binary_parse_cmd64(bin);
	else if (bin->arch == MH_MAGIC)
		ft_binary_parse_cmd32(bin);
	else if (!ft_strncmp((char*)bin->file->content, ARMAG, SARMAG))
		ft_binary_parse_arch(bin, -1);
}

static void		ft_start_nm(t_binary *bin)
{
	if (!ft_strncmp((char*)bin->file->content, ARMAG, SARMAG))
		ft_binary_parse_arch(bin, -1);
	else if (bin->arch == MH_MAGIC_64)
		ft_binary_parse_cmd64(bin);
	else if (bin->arch == MH_MAGIC)
		ft_binary_parse_cmd32(bin);
	else if (bin->arch == FAT_CIGAM)
		ft_start_fat(bin);
}

void			ft_get_binary(char *path)
{
	t_binary	*bin;
	t_nm		*nm;

	nm = (t_nm*)ft_get_sys()->get_pgm();
	bin = nm->bin;
	if (bin == NULL)
		return ;
	bin->file = ft_map_file(path);
	if (bin->file == NULL)
		return ;
	if (!bin->file || bin->file->error)
		return ;
	bin->arch = *(int*)((char*)bin->file->content);
	ft_start_nm(bin);
}
