/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:39:25 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 15:39:27 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		ft_nm_init_fn(t_nm *nm)
{
	nm->display = &ft_nm_display;
	nm->map_file = &ft_nm_map_file;
	nm->clean_file = &ft_nm_clean_file;
}

void			ft_nm_init(void)
{
	t_sys		*sys;
	t_nm		*nm;

	sys = ft_get_sys();
	nm = sys->get_pgm();
	nm->bin = ft_binary_init();
	ft_nm_init_fn(nm);
}
