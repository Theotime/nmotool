/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_display.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:38:50 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 12:43:05 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		ft_put_start_addr(t_binary *bin, uint64_t value)
{
	int			n;
	uint64_t	tmp;

	if (bin->arch == MH_MAGIC_64)
		n = 16;
	else
		n = 8;
	tmp = value;
	while (tmp)
	{
		--n;
		tmp >>= 4;
	}
	write(1, "0000000000000000", n);
	ft_puthex(value);
}

static void		ft_display_head_symbole(t_binary *bin, t_cmd *cmd, char s, \
		char u)
{
	static char		j = -1;
	static char		a = -1;
	t_sys			*sys;

	sys = ft_get_sys();
	if (j == -1)
		j = sys->sflag_active('j') + u;
	if (a == -1)
		a = sys->sflag_active('A');
	if (a)
	{
		ft_putstr(bin->file->name);
		ft_putstr(": ");
	}
	if (cmd->sect == 0 && !j)
		write(1, "                ", bin->arch == MH_MAGIC_64 ? 16 : 8);
	else if (!j)
		ft_put_start_addr(bin, cmd->value);
	if (!j)
	{
		ft_putchar(' ');
		ft_putchar(s);
		ft_putchar(' ');
	}
}

static void		ft_display_symbole(t_cmd *cmd, t_binary *bin, t_sys *sys, \
		char s)
{
	static char		u = -1;
	static char		um = -1;

	if (u == -1)
		u = sys->sflag_active('u');
	if (um == -1)
		um = sys->sflag_active('U');
	if ((u && s != 'U') || (um && s == 'U'))
		return ;
	ft_display_head_symbole(bin, cmd, s, u);
	ft_putstr(cmd->name);
	ft_putchar('\n');
}

static void		ft_nm_extract_cmds(t_binary *bin)
{
	t_el		*cur;
	t_cmd		*cmd;
	t_sys		*sys;
	char		s;

	sys = ft_get_sys();
	cur = bin->cmds->start;
	while (cur)
	{
		cmd = (t_cmd*)cur->data;
		s = ft_get_type(bin, cmd->sect, cmd->type, cmd->value);
		if (s != '?')
			ft_display_symbole(cmd, bin, sys, s);
		cur = cur->next;
	}
}

void			ft_nm_display(t_binary *bin, t_mapfile *file, char a)
{
	if (!a && ft_get_sys()->av->len > 1 && !file->error)
	{
		ft_putstr("\n");
		ft_putstr(file->name);
	}
	if (file->error)
	{
		ft_putstr(file->name);
		ft_putendl(": can't open file");
	}
	else
	{
		if (!a && ft_get_sys()->av->len > 1)
			ft_putstr(":\n");
		if (bin->arch == MH_MAGIC_64 || bin->arch == MH_MAGIC \
			|| bin->arch == FAT_CIGAM \
			|| !ft_strncmp((char*)bin->file->content, ARMAG, SARMAG))
			ft_nm_extract_cmds(bin);
	}
}
