/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:38:54 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 15:44:53 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		init_flags(t_sys *sys)
{
	sys->add_flag('A', "path-filename", 0, 0);
	sys->add_flag('j', "just-symbole", 0, 0);
	sys->add_flag('r', "revers", 0, 0);
	sys->add_flag('U', "undefined-symbole-hidden", 0, 0);
	sys->add_flag('u', "undefined-symbole-show", 0, 0);
}

int				main(int ac, char **av)
{
	t_sys		*sys;
	t_nm		*nm;
	t_el		*cur;

	sys = ft_get_sys();
	init_flags(sys);
	sys->init(ac, av, sizeof(t_nm));
	sys->check_flags();
	if (sys->av->len < 1)
		sys->die("usage : ./ft_nm <filename>");
	ft_nm_init();
	nm = sys->get_pgm();
	cur = (t_el*)sys->av->start;
	while (cur)
	{
		nm->map_file((char*)cur->data);
		nm->display(nm->bin, nm->bin->file, sys->sflag_active('A'));
		nm->clean_file();
		cur = cur->next;
	}
	return (0);
}
