/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:39:58 by triviere          #+#    #+#             */
/*   Updated: 2016/02/02 00:20:40 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void			ft_nm_map_file(char *path)
{
	t_nm		*nm;

	nm = ft_get_sys()->get_pgm();
	ft_get_binary(path);
}

void			ft_nm_clean_file(void)
{
	t_nm		*nm;
	t_el		*tmp;

	nm = ft_get_sys()->get_pgm();
	ft_map_file_free(nm->bin->file);
	nm->bin->arch = 0;
	nm->bin->text = NO_SECT;
	nm->bin->data = NO_SECT;
	nm->bin->bss = NO_SECT;
	nm->bin->k = 0;
	while (nm->bin->cmds->len)
	{
		tmp = ft_lst_slice(nm->bin->cmds);
		free(tmp);
	}
}
