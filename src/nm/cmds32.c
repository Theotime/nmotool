/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmds32.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:50:10 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 11:47:20 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		cmd_push32(struct symtab_command *sym, t_binary *bin)
{
	uint32_t			i;
	char				*stringtable;
	struct nlist		*array;
	t_cmd				*cmd;
	char				s;

	i = 0;
	array = (struct nlist*)((char*)bin->file->content + sym->symoff);
	stringtable = (char*)bin->file->content + sym->stroff;
	while (i < sym->nsyms)
	{
		if (ft_strlen(stringtable + array[i].n_un.n_strx) > 1 \
			&& ft_strpos(stringtable + array[i].n_un.n_strx, ':') < 0)
		{
			cmd = ft_cmd_init(array[i].n_sect, array[i].n_type, \
				stringtable + array[i].n_un.n_strx, array[i].n_value);
			s = ft_get_type(bin, cmd->sect, cmd->type, cmd->value);
			if (s != '?')
				ft_lst_push(bin->cmds, ft_el_init((void*)cmd));
		}
		++i;
	}
	ft_sort_cmd(ft_get_sys(), bin);
}

static void		ft_defined_segment32(t_binary *bin, struct load_command *lc)
{
	struct segment_command		*sc;
	struct section				*s;
	struct section				*cur;
	uint32_t					i;

	i = 0;
	sc = (struct segment_command*)(char*)lc;
	s = (struct section*)((char*)sc + sizeof(struct segment_command));
	cur = s;
	while (i < sc->nsects)
	{
		cur = s + i;
		if (ft_strcmp(cur->sectname, SECT_TEXT) == 0 \
			&& ft_strcmp(cur->segname, SEG_TEXT) == 0)
			bin->text = bin->k + 1;
		else if (ft_strcmp(cur->sectname, SECT_DATA) == 0 \
			&& ft_strcmp(cur->segname, SEG_DATA) == 0)
			bin->data = bin->k + 1;
		if (ft_strcmp(cur->sectname, SECT_BSS) == 0 \
			&& ft_strcmp(cur->segname, SEG_DATA) == 0)
			bin->bss = bin->k + 1;
		++bin->k;
		++i;
	}
}

void			ft_binary_parse_cmd32(t_binary *bin)
{
	char					*ptr;
	struct mach_header		*header;
	struct load_command		*lc;
	struct symtab_command	*sym;
	uint32_t				i;

	ptr = (char*)bin->file->content;
	header = (struct mach_header*)ptr;
	lc = (void*)(ptr + sizeof(*header));
	i = 0;
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT)
			ft_defined_segment32(bin, lc);
		if (lc->cmd == LC_SYMTAB)
		{
			sym = (struct symtab_command*)lc;
			cmd_push32(sym, bin);
			break ;
		}
		lc = (void*)lc + lc->cmdsize;
		++i;
	}
}
