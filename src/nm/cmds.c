/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmds.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:39:14 by triviere          #+#    #+#             */
/*   Updated: 2016/02/02 10:34:58 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static char	display_symbole_sect(t_binary *bin, uint8_t sect)
{
	unsigned char	c;

	if (sect == bin->text)
		c = 't';
	else if (sect == bin->data)
		c = 'd';
	else if (sect == bin->bss)
		c = 'b';
	else
		c = 's';
	return (c);
}

char		ft_get_type(t_binary *bin, uint8_t sect, int type, uint64_t value)
{
	unsigned char	c;

	c = type & N_TYPE;
	if (c == N_UNDF && value != 0)
		c = 'c';
	else if (c == N_UNDF && value == 0)
		c = 'u';
	else if (c == N_PBUD)
		c = 'u';
	else if (c == N_ABS)
		c = 'a';
	else if (c == N_SECT)
		c = display_symbole_sect(bin, sect);
	else if (c == N_INDR)
		c = 'i';
	else
		c = '?';
	if (type & N_EXT && c != '?')
		c -= 32;
	return (c);
}

t_cmd		*ft_cmd_init(uint8_t sect, int type, char *name, uint64_t value)
{
	t_cmd		*cmd;

	cmd = ft_memalloc(sizeof(t_cmd));
	cmd->sect = sect;
	cmd->type = type;
	cmd->value = value;
	cmd->name = name;
	return (cmd);
}

void		ft_sort_cmd(t_sys *sys, t_binary *bin)
{
	t_el		*cur;
	char		r;

	cur = bin->cmds->start;
	r = (char)sys->sflag_active('r');
	while (cur && cur->next)
	{
		if (!r && ft_strcmp(((t_cmd*)cur->data)->name, \
			((t_cmd*)cur->next->data)->name) > 0)
		{
			ft_lst_swipe(bin->cmds, cur, cur->next);
			cur = bin->cmds->start;
			continue ;
		}
		else if (r && ft_strcmp(((t_cmd*)cur->data)->name, \
				((t_cmd*)cur->next->data)->name) < 0)
		{
			ft_lst_swipe(bin->cmds, cur, cur->next);
			cur = bin->cmds->start;
			continue ;
		}
		cur = cur->next;
	}
}
