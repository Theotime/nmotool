/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arch.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 13:25:35 by triviere          #+#    #+#             */
/*   Updated: 2016/02/02 13:28:00 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		ft_display_name(t_binary *bin, char *fn_name)
{
	static char			a = -1;

	if (a == -1)
		a = ft_get_sys()->sflag_active('A');
	if (a)
		return ;
	ft_putchar('\n');
	ft_putstr(bin->file->name);
	ft_putchar('(');
	ft_putstr(fn_name);
	ft_putstr("):\n");
}

static char		*ft_get_concat_name(char *name)
{
	t_binary	*bin;
	char		*ret;
	char		*tmp;

	bin = (t_binary*)((t_nm*)ft_get_sys()->get_pgm())->bin;
	ret = ft_strdup(bin->file->name);
	tmp = ret;
	ret = ft_strjoin(tmp, ":");
	free(tmp);
	tmp = ret;
	ret = ft_strjoin(tmp, name);
	free(tmp);
	return (ret);
}

static void		ft_select_arch(char *ptr, int size, char *name)
{
	t_binary		*bin;
	static char		a = -1;

	if (a == -1)
		a = ft_get_sys()->sflag_active('A');
	bin = ft_binary_init();
	bin->file = malloc(sizeof(t_mapfile));
	if (!a)
		bin->file->name = name;
	else
		bin->file->name = ft_get_concat_name(name);
	bin->file->content = ptr;
	bin->file->size = size;
	bin->cmds = ft_lst_init();
	bin->arch = *(unsigned int*)ptr;
	if (bin->arch == MH_MAGIC_64)
		ft_binary_parse_cmd64(bin);
	else if (*(unsigned int*)ptr == MH_MAGIC)
		ft_binary_parse_cmd32(bin);
	else
		return ;
	ft_nm_display(bin, bin->file, ft_get_sys()->sflag_active('A'));
}

void			ft_binary_parse_arch(t_binary *bin, int i)
{
	struct ranlib		*lib;
	struct ar_hdr		*ar;
	t_arch				arch;
	char				*ptr;
	int					j;

	ptr = (char*)bin->file->content;
	arch.start = (void*)ptr + sizeof(struct ar_hdr) + SARMAG + 20;
	arch.st_len = *(unsigned int*)arch.start / sizeof(struct ranlib);
	lib = (struct ranlib*)(arch.start + 4);
	arch.str = (void*)lib + arch.st_len * sizeof(struct ranlib) + 4;
	arch.arr_len = *(unsigned int*)(arch.str - 4);
	arch.str += arch.arr_len;
	while (++i < (int)arch.arr_len && arch.str - ptr < bin->file->size)
	{
		if (ft_strstr(arch.str, "SYMDEF") != NULL)
			continue;
		ar = (struct ar_hdr*)arch.str;
		arch.str += sizeof(struct ar_hdr);
		j = ft_atoi(ft_strchr(ar->ar_name, '/') + 1);
		ft_display_name(bin, arch.str);
		ft_select_arch(arch.str + j, ft_atoi(ar->ar_size), arch.str);
		arch.str += j;
		arch.str += ft_atoi(ar->ar_size) - j;
	}
}
