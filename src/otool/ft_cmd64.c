/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd64.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 17:05:32 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 15:34:38 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static void		ft_extract64(t_binary *bin, struct section_64 *s)
{
	uint64_t			i;
	void				*ptr;

	i = s->offset;
	ptr = bin->file->content;
	ft_putstr(bin->file->name);
	ft_putstr(":\n");
	ft_putendl("(__TEXT,__text) section");
	while (i < (s->offset + s->size))
	{
		if (i == 0 || (i > 15 && (i - s->offset) % 16 == 0))
			ft_put_start_addr(bin, (uint64_t)(s->addr + i - s->offset));
		ft_putchar(' ');
		ft_putnhex(((char *)(ptr + s->offset))[i - s->offset] & 0xFF, 2);
		if ((i > 15 && (i - s->offset) % 16 == 15))
			ft_putstr(" \n");
		i++;
	}
	if (i % 16 > 0)
		write(1, " \n", 2);
}

static void		ft_defined_segment64(t_binary *bin, struct load_command *lc)
{
	struct segment_command_64		*sc;
	struct section_64				*s;
	struct section_64				*cur;
	uint32_t						i;

	i = 0;
	sc = (struct segment_command_64*)(char*)lc;
	s = (struct section_64*)((char*)sc + sizeof(struct segment_command_64));
	cur = s;
	(void)bin;
	while (i < sc->nsects)
	{
		cur = s + i;
		if (ft_strcmp(cur->sectname, SECT_TEXT) == 0 \
			&& ft_strcmp(cur->segname, SEG_TEXT) == 0)
			ft_extract64(bin, cur);
		++i;
	}
}

void			ft_binary_parse_cmd64(t_binary *bin)
{
	char					*ptr;
	struct mach_header_64	*header;
	struct load_command		*lc;
	uint32_t				i;

	ptr = (char*)bin->file->content;
	header = (struct mach_header_64*)ptr;
	lc = (void*)(ptr + sizeof(*header));
	i = 0;
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT_64)
			ft_defined_segment64(bin, lc);
		lc = (void*)lc + lc->cmdsize;
		++i;
	}
}
