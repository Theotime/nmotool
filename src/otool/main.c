/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:11:16 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 15:34:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

int			main(int ac, char **av)
{
	t_sys		*sys;
	t_otool		*otool;
	t_el		*cur;

	sys = ft_get_sys();
	sys->init(ac, av, sizeof(t_otool));
	sys->check_flags();
	otool = sys->get_pgm();
	if (sys->av->len < 1)
		sys->die("usage : ./ft_otool <filename>");
	ft_otool_init();
	otool = sys->get_pgm();
	cur = (t_el*)sys->av->start;
	while (cur)
	{
		otool->map_file((char*)cur->data);
		cur = cur->next;
	}
	return (0);
}
