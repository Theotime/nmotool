/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arch.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 17:05:22 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 15:33:35 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static char		*ft_get_concat_name(char *name)
{
	t_binary	*bin;
	char		*ret;
	char		*tmp;

	bin = (t_binary*)((t_otool*)ft_get_sys()->get_pgm())->bin;
	ret = ft_strdup(bin->file->name);
	tmp = ret;
	ret = ft_strjoin(tmp, "(");
	free(tmp);
	tmp = ret;
	ret = ft_strjoin(tmp, name);
	free(tmp);
	tmp = ret;
	ret = ft_strjoin(tmp, ")");
	free(tmp);
	return (ret);
}

static void		ft_select_arch(char *ptr, int size, char *name)
{
	t_binary		*bin;

	bin = ft_binary_init();
	bin->file = malloc(sizeof(t_mapfile));
	bin->file->name = ft_get_concat_name(name);
	bin->file->content = ptr;
	bin->file->size = size;
	bin->arch = *(unsigned int*)ptr;
	if (bin->arch == MH_MAGIC_64)
		ft_binary_parse_cmd64(bin);
	else if (*(unsigned int*)ptr == MH_MAGIC)
		ft_binary_parse_cmd32(bin);
	else
		return ;
}

void			ft_binary_parse_arch(t_binary *bin, int i)
{
	struct ranlib		*lib;
	struct ar_hdr		*ar;
	t_arch				arch;
	char				*ptr;
	int					j;

	ptr = (char*)bin->file->content;
	arch.start = (void*)ptr + sizeof(struct ar_hdr) + SARMAG + 20;
	arch.st_len = *(unsigned int*)arch.start / sizeof(struct ranlib);
	lib = (struct ranlib*)(arch.start + 4);
	arch.str = (void*)lib + arch.st_len * sizeof(struct ranlib) + 4;
	arch.arr_len = *(unsigned int*)(arch.str - 4);
	arch.str += arch.arr_len;
	ft_putendl("Archive : lib/libft/libft.a");
	while (++i < (int)arch.arr_len && arch.str - ptr < bin->file->size)
	{
		if (ft_strstr(arch.str, "SYMDEF") != NULL)
			continue;
		ar = (struct ar_hdr*)arch.str;
		arch.str += sizeof(struct ar_hdr);
		j = ft_atoi(ft_strchr(ar->ar_name, '/') + 1);
		ft_select_arch(arch.str + j, ft_atoi(ar->ar_size), arch.str);
		arch.str += j;
		arch.str += ft_atoi(ar->ar_size) - j;
	}
}
