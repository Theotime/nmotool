/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 17:05:36 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 15:35:16 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

void			ft_put_start_addr(t_binary *bin, uint64_t value)
{
	int			n;
	uint64_t	tmp;

	if (bin->arch == MH_MAGIC_64)
		n = 16;
	else
		n = 8;
	tmp = value;
	while (tmp)
	{
		--n;
		tmp >>= 4;
	}
	write(1, "0000000000000000", n);
	ft_puthex(value);
}

static void		ft_otool_init_fn(t_otool *otool)
{
	otool->map_file = &ft_otool_map_file;
}

void			ft_otool_map_file(char *path)
{
	t_otool		*otool;

	otool = ft_get_sys()->get_pgm();
	ft_get_binary(path);
}

void			ft_otool_init(void)
{
	t_sys		*sys;
	t_otool		*otool;

	sys = ft_get_sys();
	otool = sys->get_pgm();
	otool->bin = ft_binary_init();
	ft_otool_init_fn(otool);
}
