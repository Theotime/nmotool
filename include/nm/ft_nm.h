/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:38:57 by triviere          #+#    #+#             */
/*   Updated: 2016/02/02 13:23:25 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NM_H
# define FT_NM_H

# include <unistd.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <ar.h>
# include "libft.h"

typedef struct s_nm			t_nm;
typedef struct s_binary		t_binary;
typedef struct s_cmd		t_cmd;
typedef struct s_arch		t_arch;

typedef	enum e_btype		t_btype;

struct						s_nm
{
	t_binary				*bin;

	void					(*display)(t_binary *bin, t_mapfile *file, char a);
	void					(*map_file)(char *path);
	void					(*clean_file)(void);
};

struct						s_binary
{
	unsigned int			arch;
	t_mapfile				*file;
	t_lst					*cmds;

	unsigned char			text;
	unsigned char			data;
	unsigned char			bss;

	int						k;
};

struct						s_cmd
{
	char					*name;
	uint8_t					sect;
	int						type;
	uint64_t				value;
};

struct						s_arch
{
	char				*start;
	unsigned int		st_len;
	unsigned int		arr_len;
	char				*str;
};

char						ft_get_type(t_binary *bin, uint8_t sect, int type, \
	uint64_t value);

void						ft_nm_init(void);

void						ft_nm_display(t_binary *bin, t_mapfile *file, \
	char a);
void						ft_nm_map_file(char *path);
void						ft_nm_clean_file(void);
void						ft_sort_cmd(t_sys *sys, t_binary *bin);

void						ft_binary_parse_cmd64(t_binary *bin);
void						ft_binary_parse_cmd32(t_binary *bin);
void						ft_binary_parse_arch(t_binary *bin, int i);

void						ft_get_binary(char *path);

t_binary					*ft_binary_init(void);

t_cmd						*ft_cmd_init(unsigned char sect, int type, \
	char *name, uint64_t value);

#endif
