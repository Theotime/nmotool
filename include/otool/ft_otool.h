/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:39:03 by triviere          #+#    #+#             */
/*   Updated: 2016/02/03 13:55:52 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_OTOOL_H
# define FT_OTOOL_H

# include <unistd.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <ar.h>
# include "libft.h"

typedef struct s_otool		t_otool;
typedef struct s_binary		t_binary;
typedef struct s_arch		t_arch;

struct						s_otool
{
	t_binary				*bin;

	void					(*map_file)(char *path);
};

struct						s_binary
{
	unsigned int			arch;
	t_mapfile				*file;
};

struct						s_arch
{
	char					*start;
	unsigned int			st_len;
	unsigned int			arr_len;
	char					*str;
};

void						ft_otool_init(void);
void						ft_get_binary(char *path);
void						ft_otool_map_file(char *path);
void						ft_put_start_addr(t_binary *bin, uint64_t value);

void						ft_binary_parse_arch(t_binary *bin, int i);
void						ft_binary_parse_cmd32(t_binary *bin);
void						ft_binary_parse_cmd64(t_binary *bin);

t_binary					*ft_binary_init(void);

#endif
